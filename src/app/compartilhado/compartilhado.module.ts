import { NgModule } from "@angular/core";
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { HeaderComponent } from "./components/header/header.component";

@NgModule({
    declarations: [HeaderComponent],
    imports: [
            NzButtonModule, 
            NzIconModule,
            CurrencyMaskModule,
            NzTableModule,
            NzButtonModule,
            NzIconModule,
            NzFormModule,
            NzSelectModule,
            NzInputModule,
            NzMessageModule,
            NzToolTipModule,
            NzModalModule
        ],
    exports: [
        HeaderComponent,
        CurrencyMaskModule,
        NzTableModule,
        NzButtonModule,
        NzIconModule,
        NzFormModule,
        NzSelectModule,
        NzInputModule,
        NzMessageModule,
        NzToolTipModule,
        NzModalModule
    ]
})
export class CompartilhadoModule {

}