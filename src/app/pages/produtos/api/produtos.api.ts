import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Produtos } from "../models/Produtos";

@Injectable({ providedIn: 'root' })
export class ProdutosApi {

    private API_URL = environment.API_URL;

    constructor(private http: HttpClient) { }

    public getAllProdutos(): Observable<Produtos[]> {
        return this.http.get<Produtos[]>(`${this.API_URL}/produtos`).pipe(
            catchError(this.errorHandler)
        );
    }

    saveProduto(produto: Produtos): Observable<Produtos> {
        return this.http.post<Produtos>(`${this.API_URL}/produtos`, produto);
    }

    deleteProduto(id: number): Observable<Produtos> {
        return this.http.delete<Produtos>(`${this.API_URL}/produtos/${id}`).pipe(
            catchError(this.errorHandler)
        );
    }

    editProduto(id: number, produto: Produtos): Observable<Produtos> {
        return this.http.put<Produtos>(`${this.API_URL}/produtos/${id}`, produto);
    }

    getById(id: number): Observable<Produtos> {
        return this.http.get<Produtos>(`${this.API_URL}/produtos/${id}`);
    }

    errorHandler(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }

}