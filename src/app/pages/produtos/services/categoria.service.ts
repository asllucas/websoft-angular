import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Categorias } from "../models/Categorias";

@Injectable({ providedIn: 'root' })
export class CategoriaService {

    private API_URL = environment.API_URL;

    constructor(private http: HttpClient) { }

    getAllCategorias(): Observable<Categorias[]> {
        return this.http.get<Categorias[]>(`${this.API_URL}/categorias`).pipe(
            catchError(this.errorHandler)
        );
    }

    saveCategoria(categoria: Categorias): Observable<Categorias> {
        return this.http.post<Categorias>(`${this.API_URL}/categorias`, categoria);
    }

    errorHandler(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }


}