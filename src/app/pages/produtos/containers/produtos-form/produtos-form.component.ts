import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProdutosApi } from '../../api/produtos.api';
import { Categorias } from '../../models/Categorias';
import { ProdutosFacade } from '../../produtos.facade';
import { CategoriaService } from '../../services/categoria.service';

@Component({
  selector: 'app-produtos-form',
  templateUrl: './produtos-form.component.html',
  styleUrls: ['./produtos-form.component.scss']
})
export class ProdutosFormComponent implements OnInit {
  public produtosForm: FormGroup;
  public categorias: Categorias[];
  public formSubmitted = false;
  public isVisibleModal = false;
  public _id = '';


  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private facade: ProdutosFacade,
    private categoriaService: CategoriaService,
    private message: NzMessageService,
    private api: ProdutosApi
  ) { }

  ngOnInit(): void {
    this.iniciarForm();

    if (this.route.snapshot.params.id) {
      const id = this.route.snapshot.params.id;
      this.preencheForm(id);
    }


    this.categoriaService.getAllCategorias().subscribe(categorias => {
      this.categorias = categorias;
    });
  }

  private iniciarForm() {
    this.produtosForm = this.formBuilder.group({
      descricao: ['', [Validators.required]],
      id_categoria: [''],
      preco_compra: ['', [Validators.required]],
      preco_venda: ['', [Validators.required]],
      quantidade: ['', [Validators.required]],
      status: ["1"]
    });
  }

  submitProduto(event) {
    const id = this.route.snapshot.params.id
    if (!this.produtosForm.valid) {
      this.formSubmitted = true;
    } else if (id) {
      this.facade.updateProduto(id, this.produtosForm.value)
      this.message.create('success', "Produto atualizado com sucesso!");
      this.voltarListagem();

    }else{      
      this.facade.saveProduto(this.produtosForm.value);
      this.message.create('success', "Produto cadastrado com sucesso!");
      this.voltarListagem();
    }
  }
  
  private preencheForm(id: any) {
    this.api.getById(id).subscribe((produto: any) => {
      this._id = produto.id_produto;
      this.produtosForm.patchValue({
        descricao: produto.descricao,
        id_categoria: produto.id_categoria,
        preco_compra: produto.preco_compra,
        preco_venda: produto.preco_venda,
        quantidade: produto.quantidade
      });
    })
  }

  showModal() {
    this.isVisibleModal = true;
  }

  closeModal() {
    this.isVisibleModal = false;
  }

  public voltarListagem() {
    this.router.navigate(['/produtos'])
  }

}
