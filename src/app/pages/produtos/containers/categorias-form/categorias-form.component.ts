import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Categorias } from '../../models/Categorias';
import { ProdutosFacade } from '../../produtos.facade';


@Component({
  selector: 'app-categorias-form',
  templateUrl: './categorias-form.component.html',
  styleUrls: ['./categorias-form.component.scss']
})
export class CategoriasFormComponent implements OnInit {
 
  public categoriasform: FormGroup;
  public formSubmitted = false;
  public categoria: Categorias[];

  @Output() closeModal = new EventEmitter();
  
  constructor(private facade: ProdutosFacade,
    private formBuilder: FormBuilder,
    private message: NzMessageService,
    private router : Router) { }

  ngOnInit(): void {
    this.iniciarForm();
  }

  private iniciarForm() {
    this.categoriasform = this.formBuilder.group({
      descricao: ['', [Validators.required]],
    })
  }

  submitCategoria(event) {
    if (!this.categoriasform.valid) {
      this.formSubmitted = true;
    } else {
      this.facade.saveCategoria(this.categoriasform.value);
      this.message.create('success', "Categoria cadastrada com sucesso!");
      this.voltarListagem();
    }
  }

  voltarListagem(){
    this.router.navigate(['/produtos'])
  }
}
