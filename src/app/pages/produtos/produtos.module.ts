import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { CompartilhadoModule } from "src/app/compartilhado/compartilhado.module";
import { ProdutosListComponent } from "./components/produtos-list/produtos-list.component";
import { ProdutosFormComponent } from "./containers/produtos-form/produtos-form.component";
import { ProdutosListagemComponent } from "./containers/produtos-listagem/produtos-listagem.component";
import { ProdutosRoutingModule } from "./produtos-routing.module";
import { CategoriasFormComponent } from "./containers/categorias-form/categorias-form.component";

@NgModule({
    declarations:[
        ProdutosListComponent,
        ProdutosListagemComponent,
        ProdutosFormComponent,
        CategoriasFormComponent
    ],
    imports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CompartilhadoModule,
        ProdutosRoutingModule
    ],
})
export class ProdutosModule{

}