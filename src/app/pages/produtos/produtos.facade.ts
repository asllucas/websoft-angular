import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { NzSubmenuService } from "ng-zorro-antd/menu";
import { NzMessageService } from "ng-zorro-antd/message";
import { ProdutosApi } from "./api/produtos.api";
import { Categorias } from "./models/Categorias";
import { Produtos } from "./models/Produtos";
import { CategoriaService } from "./services/categoria.service";
import { ProdutosState } from "./state/produtos.state";

@Injectable({ providedIn: 'root' })
export class ProdutosFacade {

    public produtos$ = this.state.produtos.collection$;
    

    constructor(private state: ProdutosState,
        private router: Router,
        private api: ProdutosApi,
        private categoriaService: CategoriaService,
        private message: NzMessageService) { }


    public getProdutos() {
        this.api.getAllProdutos().subscribe((res: Produtos[]) => {
            this.state.produtos.collection = res;
        });
    }

    public saveProduto(produto: Produtos) {
        this.api.saveProduto(produto).subscribe((produto => {
            produto = produto;
        }));
        this.getProdutos();
    }

    public updateProduto(idProduto: number, produto: Produtos){
        try {
            this.api.editProduto(idProduto, produto).subscribe((prod =>{
                prod.id_produto = idProduto;
            }));
            this.getProdutos();
            
        } catch (error) {
            console.error(error);
        }
    }

    public saveCategoria(categoria: Categorias) {
        this.categoriaService.saveCategoria(categoria).subscribe((categoria => {
            categoria = categoria;
        }));
    }

    public deleteProduto(id: number) {
       
        try {
            this.api.deleteProduto(id).subscribe(idProduto => {
                this.message.create('success', 'Produto deletado com sucesso!')
                this.getProdutos();
            });
        } catch (error) {
            console.error(error);
        }
    }

    public voltarParaListagem(): void {
        this.router.navigate(['/produtos']);
    }

    public async acessarRotaCadastro() {
        this.router.navigate([`produtos/cadastro`]);
    }

    public acessarRotaEdicao(idProduto: number): void {
        this.router.navigate([`produtos/${idProduto}/alterar`]);
    }
}