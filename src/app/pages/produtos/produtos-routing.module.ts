import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProdutosFormComponent } from "./containers/produtos-form/produtos-form.component";
import { ProdutosListagemComponent } from "./containers/produtos-listagem/produtos-listagem.component";

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'produtos'
            },
            {
                path: 'produtos',
                component: ProdutosListagemComponent,
                data: {
                    title: 'Produtos'
                }
            },
            {
                path: 'cadastro',
                component: ProdutosFormComponent
            },
            {
                path: ':id/alterar',
                component: ProdutosFormComponent
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProdutosRoutingModule {

}