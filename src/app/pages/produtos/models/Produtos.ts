export interface Produtos {
    id: number;
    id_produto: number;
    id_categoria: number
    descricao: string;
    preco_compra: string;
    preco_venda: string;
    quantidade: number;
    status: number;
}