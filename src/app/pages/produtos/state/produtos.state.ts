import { Injectable } from "@angular/core";
import { StateCollection } from "src/app/compartilhado/state.collection";
import { StateDocument } from "src/app/compartilhado/state.documents";
import { Produtos } from "../models/Produtos";

@Injectable({ providedIn: 'root' })
export class ProdutosState {

    public produtos = new StateCollection<Produtos>([]);
    public carregando = new StateDocument<boolean>(true);

}