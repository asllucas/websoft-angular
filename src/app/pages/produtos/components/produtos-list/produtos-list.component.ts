import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NzButtonSize } from 'ng-zorro-antd/button';
import { Produtos } from '../../models/Produtos';

@Component({
  selector: 'app-produtos-list',
  templateUrl: './produtos-list.component.html',
  styleUrls: ['./produtos-list.component.scss']
})
export class ProdutosListComponent implements OnInit {

  size: NzButtonSize = 'small';

  @Input() produtos: Produtos[] = []!;
  @Output() editar = new EventEmitter<number>();
  @Output() remover = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }
}
