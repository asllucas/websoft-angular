import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Clientes } from "../models/clientes";

@Injectable({ providedIn: 'root' })
export class ClientesApi {

    private API_URL = environment.API_URL;

    constructor(private http: HttpClient){}

    public getAllClientes(): Observable<Clientes[]> {
        return this.http.get<Clientes[]>(`${this.API_URL}/clientes`).pipe(
            catchError(this.errorHandler)
        );
    }

    saveCliente(cliente: Clientes): Observable<Clientes> {
        return this.http.post<Clientes>(`${this.API_URL}/clientes`, cliente);
    }

    deleteCliente(id: number): Observable<Clientes> {
        return this.http.delete<Clientes>(`${this.API_URL}/clientes/${id}`).pipe(
            catchError(this.errorHandler)
        );
    }

    editCliente(id: number, cliente: Clientes): Observable<Clientes> {
        return this.http.put<Clientes>(`${this.API_URL}/clientes/${id}`, cliente);
    }

    getById(id: number): Observable<Clientes> {
        return this.http.get<Clientes>(`${this.API_URL}/clientes/${id}`);
    }

    errorHandler(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }

}