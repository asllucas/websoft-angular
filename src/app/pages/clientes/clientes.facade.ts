import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { NzMessageService } from "ng-zorro-antd/message";
import { ClientesApi } from "./api/clientes.api";
import { Clientes } from "./models/clientes";
import { ClientesState } from "./state/cliente.state";

@Injectable({ providedIn: 'root' })
export class ClientesFacade {
    public clientes$ = this.state.clientes.collection$;


    constructor(private state: ClientesState,
        private router: Router,
        private api: ClientesApi,
        private message: NzMessageService) { }


    public getClientes() {
        this.api.getAllClientes().subscribe((res: Clientes[]) => {
            this.state.clientes.collection = res;
        });
    }

    public saveCliente(cliente: Clientes) {
        this.api.saveCliente(cliente).subscribe((cliente => {
            cliente = cliente;
        }));
        this.getClientes();
    }

    public updateCliente(idCliente: number, cliente: Clientes) {
        try {
            this.api.editCliente(idCliente, cliente).subscribe((cli => {
                cli.id_cliente = idCliente;
            }));
            this.getClientes();

        } catch (error) {
            console.error(error);
        }
    }


    public deleteCliente(id: number) {

        try {
            this.api.deleteCliente(id).subscribe(idCliente => {
                this.message.create('success', 'Cliente deletado com sucesso!')
                this.getClientes();
            });
        } catch (error) {
            console.error(error);
        }
    }

    public voltarParaListagem(): void {
        this.router.navigate(['/clientes']);
    }

    public async acessarRotaCadastro() {
        this.router.navigate([`clientes/cadastro`]);
    }

    public acessarRotaEdicao(idCliente: number): void {
        this.router.navigate([`clientes/${idCliente}/alterar`]);
    }
}