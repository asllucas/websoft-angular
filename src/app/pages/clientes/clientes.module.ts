import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CompartilhadoModule } from "src/app/compartilhado/compartilhado.module";
import { ClientesListComponent } from "./components/clientes-list/clientes-list.component";
import { ClientesRoutingModule } from "./clientes-routing.module";
import { ClientesFormComponent } from "./containers/clientes-form/clientes-form.component";
import { ClientesListagemComponent } from "./containers/clientes-listagem/clientes-listagem.component";
import { ClientesDetalheComponent } from "./components/clientes-detalhe/clientes-detalhe.component";

@NgModule({
    declarations: [
        ClientesListComponent,
        ClientesListagemComponent,
        ClientesFormComponent,
        ClientesDetalheComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CompartilhadoModule,
        ClientesRoutingModule
    ]
})
export class ClientesModule {

}