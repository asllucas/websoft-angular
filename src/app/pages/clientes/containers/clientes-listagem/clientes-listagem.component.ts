import { Component, OnInit } from '@angular/core';
import { ClientesFacade } from '../../clientes.facade';

@Component({
  selector: 'app-clientes-listagem',
  templateUrl: './clientes-listagem.component.html',
  styleUrls: ['./clientes-listagem.component.scss']
})
export class ClientesListagemComponent implements OnInit {

  constructor(public facade: ClientesFacade) { }

  ngOnInit(): void {
    this.facade.getClientes();
  }

}
