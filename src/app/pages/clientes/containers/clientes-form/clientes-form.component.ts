import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ClientesApi } from '../../api/clientes.api';
import { ClientesFacade } from '../../clientes.facade';

@Component({
  selector: 'app-clientes-form',
  templateUrl: './clientes-form.component.html',
  styleUrls: ['./clientes-form.component.scss']
})
export class ClientesFormComponent implements OnInit {

  public clientesForm: FormGroup;
  public formSubmitted = false;
  public _id = '';

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private facade: ClientesFacade,
    private message: NzMessageService,
    private api: ClientesApi) { }

  ngOnInit(): void {
    this.iniciarForm();
    
    if (this.route.snapshot.params.id) {
      const id = this.route.snapshot.params.id;
      this.preencheForm(id);
    }
  }

  private iniciarForm() {
    this.clientesForm = this.formBuilder.group({
      nome: ['', [Validators.required]],
      tipo: [''],
      cnpj: ['', [Validators.required]],
      cpf: ['', [Validators.required]],
      inscricao: [''],
      endereco: [''],
      complemento: [''],
      bairro: [''],
      cep: [''],
      cidade: [''],
      uf: [''],
      telefone: [''],
      celular: ['', [Validators.required]],
      email: ['', [Validators.required]],
      status: ["1"]
    });
  }

  submit(event) {
    const id = this.route.snapshot.params.id
    if (!this.clientesForm.valid) {
      this.formSubmitted = true;
    } else if (id) {
      this.facade.updateCliente(id, this.clientesForm.value)
      this.message.create('success', "Cliente atualizado com sucesso!");
      this.voltarListagem();

    } else {
      this.facade.saveCliente(this.clientesForm.value);
      this.message.create('success', "Cliente cadastrado com sucesso!");
      this.voltarListagem();
    }
  }

  private preencheForm(id: any) {
    this.api.getById(id).subscribe((cliente: any) => {
      this._id = cliente.id_cliente;
      this.clientesForm.patchValue({
        nome: cliente.nome,
        tipo: cliente.tipo,
        cnpj: cliente.cnpj,
        cpf: cliente.cpf,
        inscricao: cliente.inscricao,
        endereco: cliente.endereco,
        complemento: cliente.complemento,
        bairro: cliente.bairro,
        cep: cliente.cep,
        cidade: cliente.cidade,
        uf: cliente.uf,
        telefone: cliente.telefone,
        celular: cliente.celular,
        email: cliente.email
      });
    })
  }

  public voltarListagem() {
    this.router.navigate(['/clientes'])
  }

}
