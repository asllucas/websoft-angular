export interface Clientes{

    id: number;
    id_cliente: number;
    nome: string;
    tipo: string;
    cnpj: string;
    cpf: string;
    inscricao: string;
    endereco : string;
    complemento : string;
    bairro: string;
    cep: string;
    cidade: string;
    uf: string;
    telefone: string;
    celular: string;
    email: string;
    status: string;
}