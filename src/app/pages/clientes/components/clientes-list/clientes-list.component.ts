import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Clientes } from '../../models/clientes';

@Component({
  selector: 'app-clientes-list',
  templateUrl: './clientes-list.component.html',
  styleUrls: ['./clientes-list.component.scss']
})
export class ClientesListComponent implements OnInit {

  @Input() clientes: Clientes[] = []!;
  @Output() editar = new EventEmitter<number>();
  @Output() remover = new EventEmitter<number>();
  @Output() info = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

}
