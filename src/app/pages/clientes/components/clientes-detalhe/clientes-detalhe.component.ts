import { Component, Input, OnInit } from '@angular/core';
import { Clientes } from '../../models/clientes';

@Component({
  selector: 'app-clientes-detalhe',
  templateUrl: './clientes-detalhe.component.html',
  styleUrls: ['./clientes-detalhe.component.scss']
})
export class ClientesDetalheComponent implements OnInit {
  
  @Input() clientes: Clientes[] = []!;

  constructor() { }

  ngOnInit(): void {
  }

}
