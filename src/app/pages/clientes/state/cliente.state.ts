import { Injectable } from "@angular/core";
import { StateCollection } from "src/app/compartilhado/state.collection";
import { StateDocument } from "src/app/compartilhado/state.documents";
import { Clientes } from "../models/clientes";

@Injectable({providedIn: 'root'})
export class ClientesState{

    public clientes = new StateCollection<Clientes>([]);
    public carregando = new StateDocument<boolean>(true);

}